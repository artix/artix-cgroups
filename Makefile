SHELL=/bin/bash

TOOLS = artix-cgroups
SYSCONFDIR = /etc
PREFIX = /usr/local
LIBDIR = $(PREFIX)/lib/artix
DATADIR = $(PREFIX)/share/artix/cgroups
BUILDDIR = build
CGROUP ?= artix
CGEXT ?= .sv

HAVECHILD = no

rwildcard=$(foreach d,$(wildcard $(1:=/*)),$(call rwildcard,$d,$2) $(filter $(subst *,%,$2),$d))

BINPROGS_SRC = $(wildcard src/*.in)
BINPROGS = $(addprefix $(BUILDDIR)/,$(patsubst src/%,bin/%,$(patsubst %.in,%,$(BINPROGS_SRC))))
LIBRARY_SRC = $(call rwildcard,src/lib,*.sh)
LIBRARY = $(addprefix $(BUILDDIR)/,$(patsubst src/%,%,$(patsubst %.in,%,$(LIBRARY_SRC))))

CONFIG_SRC = $(call rwildcard,src/conf,*.conf)
CONFIG = $(addprefix $(BUILDDIR)/,$(patsubst src/%,%,$(patsubst %.in,%,$(CONFIG_SRC))))

ifeq ($(HAVECHILD),yes)
	HAVECHILD = true
else
	HAVECHILD = false
endif

all: binprogs library config
binprogs: $(BINPROGS)
library: $(LIBRARY)
config: $(CONFIG)

edit = sed -e "s|@libdir[@]|$(DATADIR)|g"\
	-e "s|@sysconfdir[@]|$(SYSCONFDIR)|g" \
	-e "s|@cgname[@]|$(CGROUP)|g" \
	-e "s|@havechild[@]|$(HAVECHILD)|g" \
	-e "s|@cgext[@]|$(CGEXT)|g"

GEN_MSG = @echo "GEN $(patsubst $(BUILDDIR)/%,%,$@)"

RM = rm -f

define buildInScript
$(1)/%: $(2)%$(3)
	$$(GEN_MSG)
	@mkdir -p $$(dir $$@)
	@$(RM) "$$@"
	@cat $$< | $(edit) >$$@
	@chmod $(4) "$$@"
	@bash -O extglob -n "$$@"
endef

$(eval $(call buildInScript,build/bin,src/,.in,755))
$(eval $(call buildInScript,build/lib,src/lib/,,644))

$(eval $(call buildInScript,build/conf,src/conf/,,644))


clean:
	rm -rf $(BUILDDIR)

install: all
	install -dm0755 $(DESTDIR)$(LIBDIR)
	install -m0755 $(BINPROGS) $(DESTDIR)$(LIBDIR)
	install -dm0755 $(DESTDIR)$(DATADIR)
	install -dm0755 $(DESTDIR)$(SYSCONFDIR)/artix

	cp -ra $(BUILDDIR)/lib/* $(DESTDIR)$(DATADIR)

	for conf in $(notdir $(CONFIG)); do install -Dm0644 $(BUILDDIR)/conf/$$conf $(DESTDIR)$(SYSCONFDIR)/artix/$${conf##*/}; done

uninstall:
	for f in $(notdir $(BINPROGS)); do rm -f $(DESTDIR)$(LIBDIR)/$$f; done
	for f in $(notdir $(LIBRARY)); do rm -f $(DESTDIR)$(DATADIR)/$$f; done
	for conf in $(notdir $(CONFIG)); do rm -f $(DESTDIR)$(SYSCONFDIR)/$${conf##*/}; done
	rmdir --ignore-fail-on-non-empty \
		$(DESTDIR)$(DATADIR)

dist:
	git archive --format=tar --prefix=$(TOOLS)-$(V)/ v$(V) | gzip > $(TOOLS)-$(V).tar.gz
	gpg --detach-sign --use-agent $(TOOLS)-$(V).tar.gz

check: $(BINPROGS) $(LIBRARY)
	shellcheck -x $^

.PHONY: all binprogs library config clean install uninstall dist check
.DELETE_ON_ERROR:
